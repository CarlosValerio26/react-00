import React from "react";
import "./App.css";
// import "./Calendari.css";

const DIASSEMANAS = ["Dl", "Dm", "Dc", "Dj", "Dv", "Ds", "Dg"];
const MESESANIO = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Julio", "Junio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

function Mes(props) {
    return <h1>{MESESANIO[props.mes - 1]}</h1>
}

function Dia(props) {
    if (props.valor === 0) {
        return (
            <>
                <div className="dia amagat ">{props.valor}</div>
            </>
        );
    } else {
        return (
            <>
                <div style={{ color: props.festivo ? "red" : "black" }} className="dia">{props.valor}</div>
            </>
        );
    }
}

function Semana(props) {

    return (
        <>
            <div>
                {props.diaSemana.map((el, i) => (
                    <Dia festivo={i === 5 || i === 6} key={i} valor={el} />
                ))}
            </div>
        </>
    );
}

function Calendari(props) {
    //CALCULAMOS EL PRIMER Y ULTIMO DIA DEL MES
    var primerDia = new Date(props.any, props.mes - 1, 1);
    var ultimoDia = new Date(props.any, props.mes, 0);
    //CON EL getDate() OBTENEMOS LOS DIAS TOTALES DEL MES SOLICITADO
    var diasTotales = ultimoDia.getDate();
    // console.log("ultimo dia " + diasTotales);

    //COGEMOS EL PRIMER DIA DE LA SEMANA
    var primerDiaDeLaSemana = primerDia.getDay();
    // console.log("primer dia " + primerDiaDeLaSemana);

    //CON ESTA CONDICION, AVERIGUAREMOS SI EL PRIMER DIA DEL MES ES DOMINGO
    if (primerDiaDeLaSemana === 0) {
        primerDiaDeLaSemana = 7;
    }

    //CALCULAREMOS LAS SEMANAS TOTALES DE UN MES
    // var primerDiasMes = ((new Date(props.any, props.mes, 1).getDay() - 1) % 7 + 7) % 7;
    // var dias = new Date(props.any, props.mes + 1, 0).getDate() - 7 + primerDiasMes;
    // var totalSemanas = Math.ceil(dias / 7) + 1;

    //ALMACENAREMOS LOS DIAS DEL MES DEL 1-31 EN ESTE NUEVO ARRAY
    var listaDias = [];

    //AÑADIREMOS CERO DEPENDIENDO DE EN QUE DIA HAYA CAIDO EL primerDia, ES DECIR, SI EL PRIMER DIA ES MIERCOLES, ENTONCES AÑADIREMOS 2 CEROS QUE OCUPARAN LUNES Y MARTES
    //POR LO TANTO SI  primerDiaDeLaSemana ES 3, ENTONCES AÑADIRA CEROS HASTA QUE TERMINE EL BUCLE
    //
    for (var index = 1; index < primerDiaDeLaSemana; index++) {
        //CON EL push(0) AÑADIREMOS LOS CEROS A LA LISTA
        listaDias.push(0)
    }

    //LO MISMO QUE EL BUCLE ANTERIOR, SOLO QUE AÑADIRA LOS DIAS, DEPENDIENDO DEL ULTIMO DIA DEL MES
    //ES DECIR SI EL MES TIENE 28 DIAS, AÑADIRA 28 VALORES DIFERENTES ENTRE 1-28
    for (var indexDiasMes = 1; indexDiasMes <= diasTotales; indexDiasMes++) {
        //CON EL push(0) AÑADIREMOS LOS DIAS DIFERENTES A 0 A LA LISTA
        listaDias.push(indexDiasMes);
    }

    // console.log("numero semanas " + listaDias.length / 7);

    //CON EL Math.ceil REDONDEAREMOS EL RESULTADO OBTENIDO DE DIVIDIR (listaDias.length / 7), PARA OBTENER LAS SEMANAS TOTALES DEL MES ACTUAL
    //SERIA LO MISMO QUE PONER diasTotales EN VEZ DE listaDias.length
    var numeroSemanas = Math.ceil(listaDias.length / 7);
    // console.log("numero semanas " + numeroSemanas);

    //EN ESTE ARRAY ALMACENAREMOS LAS SEMANAS QUE TIENE EL MES
    var semanas = [];

    for (index = 0; index < numeroSemanas; index++) {
        //COMO CADA SEMANA TIENE 7 DIAS, CON EL SPLICE LO QUE HAREMOS ES AÑADIR 7 DIAS
        //TODO DEPENDIENDO DEL NUMERO DE SEMANS QUE HAYA
        //SI EL MES TIENE 4 SEMANAS, PUES AÑADIRA EN LA PRIMERA SEMANA 7 DIAS, DESPUES EN LA 2DA SEMANA AÑADIRA OTROS 7 DIAS
        //Y ESTO SE DEBE A QUE EL SPLICE ESTA ENTRE 0-7
        //Y CON EL lstaDias, DEPENDIENDO DEL NUMERO DE DIAS QUE CONTIENE, PUES AÑADIRA LOS NUMEROS ENTRE 0-31 NECESARIOS PARA COMPLETAR EL MES 
        var semana = listaDias.splice(0, 7);
        // console.log("semana" +semana);

        semanas.push(<Semana key={index} diaSemana={semana} />);
    }
    // console.log("semanas " + semanas.length);

    return (
        <><div>
            {/* MOSTRAMOS EL MES */}
            <Mes mes={props.mes} />
            {/* MOSTRAMOS LOS NOMBRES DE LOS DIAS, LUNES,MARTES... */}
            <Semana diaSemana={DIASSEMANAS} />
            {/* MOSTRAMOS LOS NUMEROS CORRESPONDIENTES A CADA DIA DE LA SEMANA */}
            {semanas}

        </div>
        </>
    );
}


export default Calendari;
