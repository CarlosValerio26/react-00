import React from 'react';
import { Table } from 'reactstrap';


function Taula(props) {
    //EL map NOS SIRVE PARA TRANSFORMAR A OTRO ARRAY
    //EL dades VIENE DE Taula DE LA App.js DONDE TENEMOS DECLARA UNA VARIABLE dades={MOTOS}
    const files = props.dades.map(moto => {
        const fila = (
            <tr>
                <td>{moto.marca}</td>
                <td>{moto.model}</td>
            </tr>
        );
        return fila;
    })

    return (
        <Table>
            <thead>
                <tr>
                    <th>Marca</th>
                    <th>Model</th>
                </tr>
            </thead>
            <tbody>
                {files}
            </tbody>
        </Table>
    );

}


export default Taula;