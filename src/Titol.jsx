import React, { useState } from 'react';

function Titol(props) {
    //CON EL useState HACEMOS QUE CAMBIE DE COLOR AL CLICAR CON EL MOUSE
    const [vermell, setVermell] = useState(false);
    function canviColor() {
        setVermell(!vermell);
        console.log("canviant color..." + vermell);
    }
    if (vermell) {
        // eslint-disable-next-line jsx-a11y/heading-has-content
        return <h1 onClick={canviColor} className="red"></h1>;
    } else {
        return <h1 onClick={canviColor} className="white"></h1>;
    }
}

export default Titol;
