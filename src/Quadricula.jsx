import React from "react";
import "./App.css";

function Quadrat() {
  let color = ["blue", "red", "green", "yellow"][Math.floor(Math.random() * 4)];
  return <div style={{ backgroundColor: color }} className="quadrat" />
}

function Fila(props) {
  let x = [];
  for (let index = 0; index < props.elements; index++) {
    x.push(<Quadrat key={index} />);
  }
  return <div>{x}</div>;
}

function Quadricula(props) {
  let x = [];
  for (let index = 0; index < props.files; index++) {
    x.push(<Fila key={index} elements={props.columnes} />);
  }
  return x;
}


export default Quadricula;
