import React, { useState } from 'react';
import { Button } from 'reactstrap';
import "./App.css";

// Ejercicio 1
// Círculos cambian de color al clicar
// Crear varios círculos en pantalla, cuando se clica sobre ellos deben cambiar de color.
// Guía:
// crear el círculo como un componente funcional ex) Circulo
// utilizar useState para establecer un estado true/false, 1/0…, ej) seleccionado
// utilizar el evento onClick del "div" para ejecutar la función que modifica el state
// condicionar el estilo del círculo (in-line) al valor del state

// function ReactExercicisIni() {
//     //CON EL useState HACEMOS QUE CAMBIE DE COLOR AL CLICAR CON EL MOUSE
//     const [vermell, setVermell] = useState(false);
//     function canviColor() {
//         setVermell(!vermell);
//         console.log("canviant color..." + vermell);
//     }
//     if (vermell) {
//         // eslint-disable-next-line jsx-a11y/heading-has-content
//         return <div onClick={canviColor} className="red"></div>;
//     } else {
//         return <div onClick={canviColor} className="red white"></div>;
//     }
// }

// Ejercicio 2
// Crear dos botones y un campo <input> como en la figura.
// El valor inicial del campo input deberá ser 0.
// Al clicar sobre el boton “+” debe sumar una unidad.
// Al clicar sobre el boton “-” debe restar una unidad.
// Nota: el campo <input> no debe ser editable directamente, para ello le añadimos el atributo “disabled”:    <input   ??????????     disabled=”disabled”>
// Pistas: Utilizar variables de soporte para guardar el “valoractual” del campo antes de sumar o restar. Recordar que el valor de un campo input se lee/establece con val(). Para convertir un valor de texto en número utilizamos la funcion parseInt().

// Ejercicio 2b
// Limitar los valores posibles a números enteros de 0 a 10.
// Pista: ver qué valor se establecerá si añadimos o restamos, utilizar if para actuar en consecuencia.
// function ReactExercicisIni() {
//     //CON EL useState HACEMOS QUE CAMBIE DE COLOR AL CLICAR CON EL MOUSE
//     var valorActual = 0;

//     function sumar() {
//         valorActual = valorActual + 1;
//         if (valorActual >= 0 && valorActual <= 10) {
//             //NECESITAMOS PONER EL .value PARA HACER REFERENCIA A QUE ES UN INPUT
//             document.getElementById("lname").value = valorActual;
//         }
//     }
//     function restar() {
//         valorActual = valorActual - 1;
//         //NECESITAMOS PONER EL .value PARA HACER REFERENCIA A QUE ES UN INPUT
//         if (valorActual >= 0 && valorActual <= 10) {
//             //NECESITAMOS PONER EL .value PARA HACER REFERENCIA A QUE ES UN INPUT
//             document.getElementById("lname").value = valorActual;
//         }
//     }

//     return <div className="red">
//         <button onClick={restar}>-</button>
//         <input type="text" id="lname" name="lname" disabled="disabled" />
//         <button onClick={sumar}>+</button>
//     </div>;
// }


// Ejercicio 4
// En este ejercicio utilizaremos bootstrap, por tanto deberemos cargar la librería correspondiente.
// Objetivo: deberán añadirse las clases y la funcionalidad javascript necesarias para que cuando cliquemos sobre “<<” reste una unidad a todos los números, y cuando cliquemos en “>>” sume una unidad:
function ReactExercicisIni(props) {
    //CON EL useState HACEMOS QUE CAMBIE DE COLOR AL CLICAR CON EL MOUSE
    var valorActual = 0;
    var m1;
    function sumar() {
        valorActual++;
        if (valorActual >= 0 && valorActual <= 10) {
            //NECESITAMOS PONER EL .value PARA HACER REFERENCIA A QUE ES UN INPUT
            document.getElementById("lname").value = valorActual;
            document.getElementById("lname2").value = valorActual;
            document.getElementById("lname3").value = valorActual;
        }
    }
    function restar() {
        valorActual--;
        //NECESITAMOS PONER EL .value PARA HACER REFERENCIA A QUE ES UN INPUT
        if (valorActual >= 0 && valorActual <= 10) {
            //NECESITAMOS PONER EL .value PARA HACER REFERENCIA A QUE ES UN INPUT

            document.getElementById("lname").value = valorActual;
            document.getElementById("lname2").value = valorActual;
            document.getElementById("lname3").value = valorActual;
        }
    }

    return <div className="red">
        <Button onClick={restar}> {props.resta} </Button>
        <input type="number" id="lname" name="lname" disabled="disabled" className="inputText" value={1} />
        <input type="number" id="lname2" name="lname" disabled="disabled" className="inputText" value={2} />
        <input type="number" id="lname3" name="lname" disabled="disabled" className="inputText" value={3} />
        <Button onClick={sumar}> {props.suma} </Button>
    </div>;
}
export default ReactExercicisIni;
