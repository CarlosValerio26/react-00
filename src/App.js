import React from 'react';
import Titol from './Titol';
import Quadricula from "./Quadricula";
import Taula from "./Taula";
import Calendari from "./Calendari";
import "./App.css";


function App() {
  var meses = [];
  for (let index = 1; index <= 12; index++) {
    meses.push(<Calendari mes={index} any={2021} />);
  }

  return (
    <div>{meses}</div>
  );
}


export default App;
